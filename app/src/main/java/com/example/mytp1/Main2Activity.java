package com.example.mytp1;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import static java.lang.String.valueOf;
//Seconde vue

public class Main2Activity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity2_main);

        //Récupère la valeur que l'on a envoyé ex: france
        Intent intent = getIntent();
        final String pay = intent.getStringExtra("pays");

        //récupère le nom de l'image du pays
        final CountryList cl = new CountryList();
        final String image = cl.getCountry(pay).getmImgFile();

        final Resources res = this.getResources();
        int resID = res.getIdentifier(image, "drawable", getPackageName());

        //Permet de remplir les textView établie en récupérant les ressources dans la hashmap
        ImageView iv = (ImageView)findViewById(R.id.imagePays);
        iv.setImageResource(resID);

        TextView textViewPays = (TextView) findViewById(R.id.Country);
        textViewPays.setText(pay);

        EditText cptl1 = (EditText) findViewById(R.id.capedit);
        cptl1.setText(cl.getCountry(pay).getmCapital());

        EditText lgue1 = (EditText) findViewById(R.id.langedit);
        lgue1.setText(cl.getCountry(pay).getmLanguage());

        EditText monai1 = (EditText) findViewById(R.id.monnaiedit);
        monai1.setText(cl.getCountry(pay).getmCurrency());

        //Les données de type population / area sont de type int ; il faut donc les caster

        EditText plt1 = (EditText) findViewById(R.id.popedit);
        int pop = cl.getCountry(pay).getmPopulation();
        plt1.setText(valueOf(pop));


        EditText spf1 = (EditText) findViewById(R.id.supedit);
        int ficie = cl.getCountry(pay).getmArea();
        spf1.setText(valueOf(ficie));


        //Quand on clique sur le bouton sauvegarder
        Button sauvegarder = (Button) findViewById(R.id.Sauvegarder);

        sauvegarder.setOnClickListener(new View.OnClickListener()
        {
            public void onClick(View view) {
                //Récupère les données écrites dans la textView
                EditText cptl = (EditText) findViewById(R.id.capedit);
                EditText lgue = (EditText) findViewById(R.id.langedit);
                EditText monai = (EditText) findViewById(R.id.monnaiedit);
                EditText plt = (EditText) findViewById(R.id.popedit);
                EditText spf = (EditText) findViewById(R.id.supedit);

                //On caste les donées récupéré en leurs type d'origine
                String capital = cptl.getText().toString();
                String langue = lgue.getText().toString();
                String monnaie = monai.getText().toString();
                String pop = plt.getText().toString();
                String sup = spf.getText().toString();
                int population;
                int superficie;
                try
                {
                    if (!capital.isEmpty() && !langue.isEmpty() && !monnaie.isEmpty() && !pop.isEmpty() && !sup.isEmpty())
                    {
                        //Permet de sauvegarder les données dans la base de données
                        population = Integer.parseInt(pop);
                        superficie = Integer.parseInt(sup);

                        cl.getHashMap().put(pay, new Country(capital, image, langue, monnaie, population, superficie));

                        //Permet d'afficher ce que l'on écrit.
                        Toast.makeText(Main2Activity.this, "Sauvegarde effectué",
                                Toast.LENGTH_LONG).show();


                        Intent intent = new Intent(Main2Activity.this, MainActivity.class);
                        startActivity(intent);
                    }
                    else
                    {
                        Toast.makeText(Main2Activity.this, "Un champs est vide",
                                Toast.LENGTH_LONG).show();
                    }
                }
                catch(Exception e)
                {
                    Toast.makeText(Main2Activity.this, "Erreur de saisie",
                            Toast.LENGTH_LONG).show();
                }


            }
        });
    }
}
