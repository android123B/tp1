package com.example.mytp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView;
import android.view.View;
import android.content.Intent;



public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Permet de visionner la liste
        final ListView listview = (ListView) findViewById(R.id.listview);
        CountryList cl = new CountryList();
        String[] values = cl.getNameArray();

        // ArrayAdapter, met le Strings dans ListView
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);


        //Quand on va cliquer sur le bouton, envoie les données à la vue suivante
        //String select = (String) parent.getItemAtPosition(position) -> permet de connaitre le string que l'on vient de cliquer

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView parent, View v, int position, long id)
            {
                // Do something in response to the click
                String select = (String) parent.getItemAtPosition(position);
                Intent intent = new Intent(MainActivity.this, Main2Activity.class);
                intent.putExtra("pays", select);
                startActivity(intent);

            }
        });


    }

}
